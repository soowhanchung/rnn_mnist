import tensorflow as tf
import numpy as np
import fileio as fio
from tensorflow.contrib import rnn

def fc_layer_init_xavier(in_node, dims, act_fn=tf.identity, name='fc'):
  """
  Fully connected feed-forward layer with randomly initialized network
  :param in_node : input node
  :param dims    : [input_dims, output_dims]
  :param act_fn  : activation function
  :param name    : name of the scope
  :return        : activated neural nodes
  """
  stddev = np.reciprocal(np.sqrt((dims[0] + 1) * dims[1]))
  with tf.name_scope(name):
    w = tf.Variable(tf.random_normal([dims[0], dims[1]], stddev=stddev))
    b = tf.Variable(tf.random_normal([dims[1]], stddev=stddev))
    return act_fn(tf.add(tf.matmul(in_node, w), b)), [w, b]


def dnn_model_init_xavier(in_node, out_node, dims, act_fn=tf.identity, out_fn=tf.identity, name='dnn'):
    # data dimensions
    data_dim = [in_node.get_shape()[1].value, out_node.get_shape()[1].value]
    # size of layer
    layer_size = [data_dim[0]]
    layer_size.extend(dims)
    layer_size.append(data_dim[1])
    # define model

    # model settings - not liking fixed number of layers and range
    network = []
    h, net = fc_layer_init_xavier(in_node, layer_size[0:2], act_fn, 'in')
    network.append(net)
    for i in range(1, len(dims)):
      h, net = fc_layer_init_xavier(h, layer_size[i:i+2], act_fn)
      network.append(net)
    y, net = fc_layer_init_xavier(h, layer_size[i+1:i+3], out_fn, 'out')
    network.append(net)



def lstm_cell(size):
  with tf.name_scope('lstm_cell'):
    return rnn.BasicLSTMCell(size)


def rnn_model_init_xavier(in_node, out_node, dims, act_fn=tf.identity, out_fn=tf.identity, name='lstm'):

    # Prepare data shape to match `rnn` function requirements
    # Current data input shape: (batch_size, n_steps, n_input)
    # Required shape: tensors list of shape (batch_size, n_input)
    stddev = np.reciprocal(np.sqrt((dims[-2] + 1) * dims[-1]))
    w = tf.Variable(tf.random_normal([dims[-2], dims[-1]], stddev=stddev))
    b = tf.Variable(tf.random_normal([dims[-1]], stddev=stddev))

    # Define a lstm cell with tensorflow
    cell = lstm_cell(dims[0])

    stacked_lstm = tf.contrib.rnn.MultiRNNCell( \
            [lstm_cell(dims[i]) for i in range(1, len(dims))])
    initial_state = state = stacked_lstm.zero_state(batch_size, tf.float32)

      # Get lstm cell output
    y, states = rnn.static_rnn(cell, in_node, dtype=tf.float32)

    # Activation, using rnn inner loop last output
    return out_fn(tf.matmul(y[-1], w) + b), [w, b]


def cross_entropy(true, esti, name=''):
  with tf.name_scope('xent.' + name):
    xent = tf.nn.softmax_cross_entropy_with_logits(labels=true, logits=esti)
    return tf.reduce_mean(xent)


def accuracy(true, esti, name=''):
  with tf.name_scope('accuracy.' + name):
    pred = tf.equal(tf.argmax(true, 1), tf.argmax(esti, 1))
    return tf.reduce_mean(tf.cast(pred, tf.float32)) * 100


def mean_square_error(true, esti, name=''):
  """
  Mean Square Error Cost Function - in case other criteria are required
  :param true: true data
  :param esti: estimated data
  :param name: name of the scope
  :return: evaluated mse tensors
  """
  with tf.name_scope('mse.' + name):
    return (1/2) * tf.reduce_mean(tf.square(tf.subtract(true, esti)))


def mean_cost(x, name=''):
  with tf.name_scope('cost_' + name):
    return tf.reduce_mean(x)


def gen_minibatch_idx(total, size):
  """
  Minibatch index generator
  :param total: total number of data
  :param size: size of minibatch
  :return: non-overlapping indices ranging from 0 to total, in minibatch size
  """
  n_minibatch = total // size
  mix_idx = np.random.permutation(total)
  minibatch_idx = mix_idx[:n_minibatch * size]
  minibatch_idx.shape = (n_minibatch, size)
  return minibatch_idx


def gen_model(name, dims, in_node, out_node, act_fn, out_fn, cost_fn, metric_fn, lr=0.01, optimizer='SGD'):
  """
  Generating Fully-connected DNN Model
  :param name: name scope of this model
  :param dims: dimension of hidden layers (3 hidden layers)
  :param in_node: placeholder for input data
  :param out_node: placeholder for output data
  :param act_fn: activation functions for hidden layers
  :param cost_fn: output cost functions
  :return: list of model related variables
  """
  # data dimensions
  data_dim = [in_node.get_shape()[1].value, out_node.get_shape()[1].value]
  # size of layer
  layer_size = [data_dim[0]]
  layer_size.extend(dims)
  layer_size.append(data_dim[1])
  # define model

  a = dims[-2:len(dims)]
  with tf.name_scope(name):
    # model settings - not liking fixed number of layers and range
    network = []
    h, net = fc_layer_init_xavier(in_node, layer_size[0:2], act_fn, 'in')
    network.append(net)
    for i in range(1, len(dims)):
      h, net = fc_layer_init_xavier(h, layer_size[i:i+2], act_fn)
      network.append(net)
    y, net = fc_layer_init_xavier(h, layer_size[i+1:i+3], out_fn, 'out')
    network.append(net)

    cost = cost_fn(out_node, y, name)
    accuracy = metric_fn(out_node, y, name)
    if optimizer == 'SGD':
      bp = tf.train.GradientDescentOptimizer(name='opt', learning_rate=lr).minimize(cost)
    if optimizer == 'Adam':
      bp = tf.train.AdamOptimizer(name='opt', learning_rate=lr).minimize(cost)
    if optimizer == 'Adadelta':
      bp = tf.train.AdadeltaOptimizer(name='opt', learning_rate=lr).minimize(cost)
    if optimizer == 'Adagrad':
      bp = tf.train.AdagradOptimizer(name='opt', learning_rate=lr).minimize(cost)
  return [in_node, out_node, y, data_dim, cost, accuracy, bp, network]


def save_network_model(session, network, filename):
  fid = open(filename, 'wb')
  fid.write(len(network).to_bytes(4, 'little'))

  for layer in network: # each layer 1: weight 2: bias
    fid.write(len(layer).to_bytes(4, 'little'))
    for weight in layer:

      # for each network weight (could also be bias) item in layer
      weight_array = weight.eval(session)
      fid.write(len(weight_array.shape).to_bytes(4, 'little'))
      # print(len(weight_array.shape), np.array(weight_array.shape))
      fid.write(np.array(weight_array.shape, dtype=np.int32).tobytes())
      fid.write(weight_array.tobytes())
  fid.close()


def read_network_model(filename):
  fid = open(filename, 'rb')
  network = int.from_bytes(fid.read(4), 'little')  # 1
  value = []
  for net in range(network):
    nlayer = int.from_bytes(fid.read(4), 'little')    # 2

    ndata = int.from_bytes(fid.read(4), 'little')   # 3
    shape = fio.read_binary_data(fid, ndata=ndata, dtype=np.int32) # 4
    product = shape[0] * shape[1]
    weight = fio.read_binary_data(fid, ndata=product, dtype=np.float32)
    weight = np.reshape(weight, [shape[0], shape[1]])
    value.append(weight)

    ndata = int.from_bytes(fid.read(4), 'little')   # 3
    shape = fio.read_binary_data(fid, ndata=ndata, dtype=np.int32) # 4
    weight = fio.read_binary_data(fid, ndata=shape[0], dtype=np.float32)
    value.append(weight)

  fid.close()
  return value

def read_norm_vector(filename):
  fid = open(filename, 'rb')
  value = fio.read_binary_data(fid, ndata=-1, dtype=np.single)

  return value