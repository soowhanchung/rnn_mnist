import tensorflow as tf
import numpy as np
import fileio as fio
from tensorflow.contrib import rnn
import rnn_model as rm
# import common
import os
import glob
# Import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

""" MODULES TO SIMPLIFY LIFE """
def get_batch_file_dict(data_types, data_sets, basepath):
  batch_file_dict = dict()
  for dtype in data_types:
    batch_file_dict_dtype = dict()
    for dset in data_sets:
      batch_file_dict_dtype[dset] = \
        sorted(glob.glob(basepath + os.sep + dset + os.sep + dtype + '*.bin'))
    batch_file_dict[dtype] = batch_file_dict_dtype
  return batch_file_dict


def batch_file_dict_permute(in_dict, out_dict, subset):
  batch_file_comb = dict()
  for in_key in in_dict:
    for out_key in out_dict:
      dict_key = in_key + '_' + out_key      # dictionary key
      subset_dict = dict()
      for set_key in subset:
        in_list = in_dict[in_key][set_key]
        out_list = out_dict[out_key][set_key]
        comb_list = []
        for (in_file, out_file) in zip(in_list, out_list):
          comb_list.append([in_file, out_file])
        subset_dict[set_key] = comb_list
      batch_file_comb[dict_key] = subset_dict
  return batch_file_comb


def eval_bpop_per_batch(session, x, y, bp_op, batch, minibatch):

  for batch_idx in np.random.permutation(len(batch)):
    in_file = batch[batch_idx][0]
    out_file = batch[batch_idx][1]
    in_batch = fio.read_batch_feature_as_rank(in_file, dtype=np.single)
    out_batch = fio.read_batch_feature_as_rank(out_file, dtype=np.single)

    for idx in rm.gen_minibatch_idx(len(in_batch), minibatch):
      # batch_x = [in_batch[i] for i in idx]
      batch_x = in_batch[idx]
      # batch_y = [out_batch[i] for i in idx]
      batch_y = out_batch[idx]
      batchsize, n_step, n_input = batch_y.shape
      batch_y = batch_y.reshape((batchsize*n_step, n_input))

      session.run(bp_op, feed_dict={x: batch_x, y: batch_y})


def eval_cost_per_batch(session, x, y, cost, acc, batch, subset, epoch):

  for sub in subset:
    sub_batch = batch[sub]
    total_cost = 0
    total_acc = 0
    total_frame = 0
    print('      Evaluating %s subset\n' % sub, end='', flush=True)
    for batch_idx in range(len(sub_batch)):
      in_file = sub_batch[batch_idx][0]
      out_file = sub_batch[batch_idx][1]
      in_utt = fio.read_batch_feature_as_rank(in_file, dtype=np.single)
      out_utt = fio.read_batch_feature_as_rank(out_file, dtype=np.single)

      # print('\b+', end='', flush=True)
      for iutt in range(len(in_utt)):
        total_frame += len(in_utt[iutt])
        batch_x = in_utt[iutt]
        batch_y = out_utt[iutt]
        batch_x = batch_x.reshape([1, batch_x.shape[0], batch_x.shape[1]])

        utt_cost = session.run(cost, feed_dict={x: batch_x, y: batch_y})
        utt_acc = session.run(acc, feed_dict={x: batch_x, y: batch_y})
        total_cost = total_cost + utt_cost * len(in_utt[iutt])
        total_acc = total_acc + utt_acc * len(in_utt[iutt])

    _cost = total_cost / total_frame
    _acc = total_acc / total_frame

  return _cost, _acc


def fc_layer_init_xavier(in_node, dims, act_fn=tf.identity, name='fc'):
  """
  Fully connected feed-forward layer with randomly initialized network
  :param in_node : input node
  :param dims    : [input_dims, output_dims]
  :param act_fn  : activation function
  :param name    : name of the scope
  :return        : activated neural nodes
  """
  stddev = np.reciprocal(np.sqrt((dims[0] + 1) * dims[1]))
  with tf.name_scope(name):
    w = tf.Variable(tf.random_normal([dims[0], dims[1]], stddev=stddev))
    b = tf.Variable(tf.random_normal([dims[1]], stddev=stddev))
    return act_fn(tf.add(tf.matmul(in_node, w), b)), [w, b]


def lstm_cell(size, forget_bias=1.0, act_fn='tf.tanh'):
  with tf.name_scope('lstm_cell'):
    return rnn.BasicLSTMCell(size, forget_bias = forget_bias, activation = act_fn)


def cross_entropy(true, esti, name=''):
  with tf.name_scope('xent.' + name):
    xent = tf.nn.softmax_cross_entropy_with_logits(labels=true, logits=esti)
    return tf.reduce_mean(xent)


def accuracy(true, esti, name=''):
  with tf.name_scope('accuracy.' + name):
    pred = tf.equal(tf.argmax(true, 1), tf.argmax(esti, 1))
    return tf.reduce_mean(tf.cast(pred, tf.float32)) * 100


def main():
  ""
  """ MANUALLY CONFIGURED GLOBAL SYSTEM SETTINGS """
  # simulation settings\
  model_dir = './model'
  logfile_dir = './logfile'

  # base_path = input("Please enter path where database exists: ")
  base_path = '/media/soowhan/external2'
  feat_subpath = 'seeds' + os.sep + 'feat'

  feat_dir = base_path + os.sep + feat_subpath
  max_epoch = 1000
  minibatch_size = 2  # 100 items per minibatch
  # training settings
  act_fn = tf.nn.relu
  out_fn = tf.identity
  cost_fn = cross_entropy
  metric_fn = accuracy
  architecture = [512, 512, 512]
  optimizer = 'Adam'
  learning_rate = 0.0001

  # database settings
  subset_types = ['TRAIN', 'TEST']              # name of data subset
  in_data_dims = 28                     # dimension of input data
  out_data_dims = 10    # dimension of output data
  n_steps = 28

  """ DNN SETTINGS """
  in_node = tf.placeholder(tf.float32, [None, n_steps, in_data_dims], 'in')
  out_node = tf.placeholder(tf.float32, [None, out_data_dims], 'out')

  batch_size = in_node.get_shape()[0]
  n_step = in_node.get_shape()[1]

  # size of layer
  layer_size = [in_data_dims]
  layer_size.extend(architecture)
  layer_size.append(out_data_dims)
  # define model
  with tf.name_scope('model'):
    # Define a lstm cell with tensorflow
    stack_cell = tf.contrib.rnn.MultiRNNCell( \
      [lstm_cell(architecture[i], act_fn=act_fn) for i in range(len(architecture))])
    # initial_state = state = stack_cell.zero_state(None, tf.float32)
    # outputs, state = tf.nn.dynamic_rnn(
    #   stack_cell,
    #   in_node,
    #   sequence_length=n_step,
    #   dtype=tf.float32)
    outputs, states = rnn.static_rnn(stack_cell, in_node, dtype=tf.float32)
    outputs = tf.reshape(outputs, [-1, architecture[-1]])
    y, net = fc_layer_init_xavier(outputs, [architecture[-1], out_data_dims[1]], act_fn=out_fn)

    cost = cost_fn(out_node, y, 'cost')
    metric = metric_fn(out_node, y, 'accuracy')
    if optimizer == 'SGD':
      bp_opt = tf.train.GradientDescentOptimizer(name='opt', learning_rate=learning_rate).minimize(cost)
    if optimizer == 'Adam':
      bp_opt = tf.train.AdamOptimizer(name='opt', learning_rate=learning_rate).minimize(cost)
    if optimizer == 'Adadelta':
      bp_opt = tf.train.AdadeltaOptimizer(name='opt', learning_rate=learning_rate).minimize(cost)
    if optimizer == 'Adagrad':
      bp_opt = tf.train.AdagradOptimizer(name='opt', learning_rate=learning_rate).minimize(cost)


  # initialize session
  sess = tf.Session()
  sess.run(tf.global_variables_initializer())


  for epoch in range(1, max_epoch + 1):
    for ibatch in range(int(60000/minibatch_size)):
      batch_x, batch_y = mnist.train.next_batch(minibatch_size)
      batch_x = batch_x.reshape((batch_size, n_step, in_data_dims))
      sess.run(bp_opt, feed_dict={in_node: batch_x, out_node: batch_y})


  # [xent, acc] = eval_cost_per_batch(sess, in_node, out_node, batch, subset_types, epoch)

  # print('[xent] Training  : %.10f' % xent[subset_types[0]][epoch])
  # print('       Validation: %.10f\n' % xent[subset_types[1]][epoch])
  # print('[acc]  Training  : %.10f' % acc[subset_types[0]][epoch])
  # print('       Validation: %.10f\n' % acc[subset_types[1]][epoch])
  #
  # # epoch-wise execution
  # for epoch in range(1, max_epoch + 1):
  #   print('  Epoch: %6d / %6d' % (epoch, max_epoch))
  #
  #   # per-model training
  #   batch = batch_file[subset_types[0]]
  #   eval_bpop_per_batch(sess, in_node, out_node, bp_opt, batch, minibatch_size)
  #
  #   # per-model evaluation
  #   batch = batch_file
  #   [xent, acc] = \
  #     eval_cost_per_batch(sess, in_node, out_node, cost, metric, batch, subset_types, epoch)
  #   print('[xent] Training  : %.10f' % xent[subset_types[0]])
  #   print('       Validation: %.10f\n' % xent[subset_types[1]])
  #   print('[acc]  Training  : %.10f' % acc[subset_types[0]])
  #   print('       Validation: %.10f\n' % acc[subset_types[1]])

  sess.close()

main()