import numpy as np
import tensorflow as tf


def read_binary_data(fid, nskip=0, ndata=-1, dtype='f'):
  fid.seek(nskip, 1)
  return np.fromfile(fid, dtype=np.dtype(dtype), count=ndata)


def read_batch_feature_as_list(filename, dtype='f'):
  fid = open(filename, 'rb')

  feature = []
  nbatch = int.from_bytes(fid.read(4), 'little')
  ndim = int.from_bytes(fid.read(4), 'little')
  for ibatch in range(nbatch):
    nframe = int.from_bytes(fid.read(4), 'little')
    ndata = nframe * ndim
    raw_data = read_binary_data(fid, ndata=ndata, dtype=dtype)
    raw_data.shape = (nframe, ndim)
    feature.append(raw_data)
  fid.close()
  return feature


def read_batch_feature_as_array(filename, dtype='f'):
  fid = open(filename, 'rb')

  # obtain full-size of the batch
  nbatch = int.from_bytes(fid.read(4), 'little')
  ndim = int.from_bytes(fid.read(4), 'little')
  nframe_total = 0
  raw_data = []
  for ibatch in range(nbatch):
    # file size measuring sequence
    nframe = int.from_bytes(fid.read(4), 'little')
    raw_data.extend(fid.read(4 * ndim * nframe))

    nframe_total += nframe
  data = np.frombuffer(bytes(raw_data), dtype=dtype)
  data.shape = (nframe_total, ndim)

  # finalize
  fid.close()
  return data

def read_batch_information(filename, dtype='f'):
  fid = open(filename, 'rb')

  max_step = 0
  feature = []
  nbatch = int.from_bytes(fid.read(4), 'little')
  ndim = int.from_bytes(fid.read(4), 'little')
  for ibatch in range(nbatch):
    nframe = int.from_bytes(fid.read(4), 'little')
    max_step = np.maximum(max_step, nframe)
    ndata = nframe * ndim
    read_binary_data(fid, ndata=ndata, dtype=dtype)
  fid.close()
  return np.zeros([nbatch, max_step, ndim], dtype='f')


def read_batch_feature_as_rank(filename, dtype='f'):
  fid = open(filename, 'rb')
  feature = read_batch_information(filename, dtype)

  nbatch = int.from_bytes(fid.read(4), 'little')
  ndim = int.from_bytes(fid.read(4), 'little')
  for ibatch in range(nbatch):
    nframe = int.from_bytes(fid.read(4), 'little')

    ndata = nframe * ndim
    raw_data = read_binary_data(fid, ndata=ndata, dtype=dtype)
    raw_data.shape = (nframe, ndim)
    feature[ibatch, 0:nframe, :] = raw_data

  fid.close()
  return feature


def convert_feature_list_to_array(feat_list):
  feat_array = []
  nfrm = 0
  for ibatch in range(len(feat_list)):
    # for istep in range(len(feat_list[ibatch])):
    #   data = feat_list[ibatch][istep]
    #   feat_array.extend(data)
    #   nfrm = nfrm + len(data)
    feat_array.extend(list(feat_list[ibatch]))
    # feat_array = tf.stack(feat_array, feat_list[ibatch])
  # ndim = len(feat_list[ibatch][istep])
  # feat_array.shape = (nfrm, ndim)
  feat_array = tf.stack(feat_array)
  return (feat_array)


def save_numpy_as_binary(filename, data):
  fid = open(filename, 'wb')
  fid.write(data.tobytes())
  fid.close()
