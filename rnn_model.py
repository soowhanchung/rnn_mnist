import tensorflow as tf
import numpy as np
import fileio as fio
from tensorflow.contrib import rnn

import tensorflow as tf
from tensorflow.contrib import rnn
import numpy as np

def fc_layer_init_xavier(in_node, dims, act_fn=tf.identity, name='fc'):
  """
  Fully connected feed-forward layer with randomly initialized network
  :param in_node : input node
  :param dims    : [input_dims, output_dims]
  :param act_fn  : activation function
  :param name    : name of the scope
  :return        : activated neural nodes
  """
  stddev = np.reciprocal(np.sqrt((dims[0] + 1) * dims[1]))
  with tf.name_scope(name):
    w = tf.Variable(tf.random_normal([dims[0], dims[1]], stddev=stddev))
    b = tf.Variable(tf.random_normal([dims[1]], stddev=stddev))
    return act_fn(tf.add(tf.matmul(in_node, w), b)), [w, b]


def lstm_cell(size, forget_bias=1.0, act_fn='tf.tanh'):
  with tf.name_scope('lstm_cell'):
    return rnn.BasicLSTMCell(size, forget_bias = forget_bias, activation = act_fn)


def length(sequence):
  used = tf.sign(tf.reduce_max(tf.abs(sequence), 2))
  length = tf.reduce_sum(used, 1)
  length = tf.cast(length, tf.int32)
  return length


def gen_model(name, dims, in_node, out_node, act_fn, out_fn, cost_fn, metric_fn, max_length = 100, lr=0.01, optimizer='SGD'):
  """
  Generating Fully-connected DNN Model
  :param name: name scope of this model
  :param dims: dimension of hidden layers (3 hidden layers)
  :param in_node: placeholder for input data
  :param out_node: placeholder for output data
  :param act_fn: activation functions for hidden layers
  :param cost_fn: output cost functions
  :return: list of model related variables
  """
  # data properties
  batch_size = in_node.get_shape()[0]
  n_step = in_node.get_shape()[1]
  a = out_node.get_shape()[1].value
  data_dim = [in_node.get_shape()[2].value, out_node.get_shape()[1].value]

  # size of layer
  layer_size = [data_dim[0]]
  layer_size.extend(dims)
  layer_size.append(data_dim[1])
  # define model
  with tf.name_scope(name):
    # in_node = tf.unstack(in_node, axis=1)

    network = []
    # Define a lstm cell with tensorflow
    stack_cell = tf.contrib.rnn.MultiRNNCell( \
            [lstm_cell(dims[i], act_fn=act_fn) for i in range(len(dims))])
    # initial_state = state = stack_cell.zero_state(None, tf.float32)
    outputs, state = tf.nn.dynamic_rnn(
      stack_cell,
      in_node,
      sequence_length=length(in_node),
      dtype=tf.float32)
    outputs = tf.reshape(outputs, [-1, dims[-1]])
    y, net = fc_layer_init_xavier(outputs, [dims[-1], data_dim[1]], act_fn=act_fn)
    network.append(stack_cell)
    network.append(net)

    cost = cost_fn(out_node, y, name)
    accuracy = metric_fn(out_node, y, name)
    if optimizer == 'SGD':
      bp = tf.train.GradientDescentOptimizer(name='opt', learning_rate=lr).minimize(cost)
    if optimizer == 'Adam':
      bp = tf.train.AdamOptimizer(name='opt', learning_rate=lr).minimize(cost)
    if optimizer == 'Adadelta':
      bp = tf.train.AdadeltaOptimizer(name='opt', learning_rate=lr).minimize(cost)
    if optimizer == 'Adagrad':
      bp = tf.train.AdagradOptimizer(name='opt', learning_rate=lr).minimize(cost)
  return [in_node, out_node, y, data_dim, cost, accuracy, bp, network]


def gen_minibatch_idx(total, size):
  """
  Minibatch index generator [batchsize x n_step x dimension]
  :param total: total number of data
  :param size: size of minibatch
  :return: non-overlapping indices ranging from 0 to total, in minibatch size
  """
  n_minibatch = total // size
  mix_idx = np.random.permutation(total)
  minibatch_idx = mix_idx[:n_minibatch * size]
  minibatch_idx.shape = (n_minibatch, size)
  return minibatch_idx

