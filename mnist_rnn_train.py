import tensorflow as tf
import numpy as np

def binary_file_read(filepath, dtype, dim=1, ndata=-1, nskip=0):
    fid = open(filepath, 'rb')
    fid.seek(nskip, 1)
    data = np.fromfile(fid, dtype=np.dtype(dtype), count=ndata)
    frame = data.size // dim
    data.shape = (frame, dim)
    return data

def read_mnist_image(filepath):
    raw_image = binary_file_read(filepath, 'u1', 28 * 28, nskip=16)
    return raw_image.astype('f') / 255

nrow = 28
ncol = 28
ndim = nrow * ncol

ndim_step = ncol
ndim_in = nrow
ndim_out = 10

image = read_mnist_image('train-images.idx3-ubyte')
label = binary_file_read('train-labels.idx1-ubyte', 'u1', nskip=8)

image_tr = image[0 : 50000]
label_tr = label[0 : 50000]

image_dv = image[50000 : 60000]
label_dv = label[50000 : 60000]

image_ev = read_mnist_image('t10k-images.idx3-ubyte')
label_ev = binary_file_read('t10k-labels.idx1-ubyte', 'u1', nskip=8)

image_tr.shape = (50000, nrow, ncol)
image_dv.shape = (10000, nrow, ncol)
image_ev.shape = (10000, nrow, ncol)

in_node = tf.placeholder(tf.float32, [None, 784])
out_node = tf.placeholder(tf.float32, [None, 10])

# w = tf.Variable(tf.random_normal([784, 10]))
# b = tf.Variable(tf.random_normal([10]))
# model_out = tf.add(tf.matmul(in_node, w), b)

# xent = tf.nn.softmax_cross_entropy_with_logits(logits=out_node, labels=model_out)
# cost = tf.reduce_mean(xent)
# opt = tf.train.AdamOptimizer().minimize(cost)

in_node = tf.placeholder(tf.float32, [None, ndim_step, ndim_in])
out_node = tf.placeholder(tf.float32, [None, ndim_out])

lstm_cell = tf.contrib.rnn.BasicLSTMCell(512)
outputs, states = tf.nn.dynamic_rnn(lstm_cell, in_node, 
                                    sequence_length=[ndim_step],
                                    dtype=tf.float32)
lstm_out = outputs[:, -1, :]

a = outputs.graph
print(dir(a))
q = a.get_all_collection_keys()
print(q)
r = a.get_collection('variables')
print(r)


w = tf.Variable(tf.random_normal([512, 10]))
b = tf.Variable(tf.random_normal([10]))
model_out = tf.add(tf.matmul(lstm_out, w), b)



